# README #

CATHY main repository. MP

DISCLAIMER

CATHY (CATchment HYdrology) is an open-source research code, written in Fortran, for simulating coupled
surface and subsurface hydrological processes involving water flow and solute transport.
The code is provided on the understanding that it should be used ONLY for research purposes.
The persons to contact for additional information are:
Matteo Camporese (University of Padova, Italy; matteo.camporese@unipd.it);
Stefano Orlandini (University of Modena and Reggio Emilia, Italy; orlandinis@unimore.it);
Claudio Paniconi (INRS-ETE, Université du Québec, Canada; claudio.paniconi@ete.inrs.ca);
Mario Putti (University of Padova, Italy; putti@math.unipd.it).

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact